﻿using UnityEngine;
using System.Collections;

public class GoalHandler : MonoBehaviour {

    public SwipeThrow swipeThrow;
	
	void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.goal = true;
        }

    }
}
