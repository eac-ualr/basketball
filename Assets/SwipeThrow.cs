﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SwipeThrow : MonoBehaviour
{

    public GameObject Player;
    public GameObject Goal;
    public Transform shootPosition;
    public GameObject Ball;
    public float throwScaler = 1.8f;
    public float averageSwipePercentage = .65f;
    public float averageSwipeDuration = .15f;
    public Camera camera;
    public Slider slider;
    public Text goalText;


    public bool goal = false;
    public Text debug;
    public Text debug2;
    public Text debug3;
    public Text debug4;


    //The position where the swipe begins (when the swipe starts moving up for the last time)
    private Vector2 touchBegin;
    //where the swipe ends
    private Vector2 touchEnd;
    //Time where the swipe begins
    private float startTime;
    //The tim when the swipe ends.
    private float endTime;
    //The time the finger leaves the screen. Used to reset ball.
    private float releaseTime;
    //The velocity of the swipe
    private float touchVelocity;
    //The position of the touch last frame, to find the bottom of the swipe.
    private Vector3 previousPosition;
    //Storing whether the ball is reset (reset = true) or in motion (rest = false)
    private bool Reset = true;



    void Update()
    {
        Debug.Log(Reset);


        if(goal == true)
        {
            goalText.text = "GOAL!";

        }else
        {
            goalText.text = "";
        }
        //Android Debugging
        debug.text = GetTimeSinceThrown().ToString();
        debug2.text = "Goal = "+goal.ToString();
        debug3.text = "Reset = " + Reset.ToString();

        //If it has been 4 seconds since the throw and they have not made a goal, reset the ball. 
        if (GetTimeSinceThrown() > 4  && Reset == false) //&& goal != true
        {
            BallReset();
        }

        if(GetTimeSinceThrown() > 10)
        {
            if(Reset != true)
            {
                BallReset();
            }
            releaseTime = Time.time;
        }

#if UNITY_EDITOR
        if (Reset == true)
        {
            if (Input.GetMouseButtonDown(0) == true)
            {
                Ray start = camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(start, out hit))
                {
                    if (hit.collider.gameObject == Ball)
                    {
                        touchBegin = Input.mousePosition;
                        startTime = Time.time;
                        previousPosition = Input.mousePosition;
                    }
                }
            }

            if (Input.GetMouseButton(0) == true)
            {
                slider.value = GetPower();
                if (Input.mousePosition.y < previousPosition.y)
                {
                    previousPosition = Input.mousePosition;
                    touchBegin = Input.mousePosition;
                    startTime = Time.time;
                }
                else
                {
                    previousPosition = Input.mousePosition;
                }
            }

            if (Input.GetMouseButtonUp(0) == true)
            {
                releaseTime = Time.time;
                touchEnd = Input.mousePosition;
                endTime = Time.time;
                touchVelocity = GetVelocity();
                slider.value = GetPower();
                if (GetPower() > .7 && GetPower() < 1.3)
                {
                    AutoShoot();

                }
                else
                {
                    Ball.GetComponent<Rigidbody>().useGravity = true;
                    Ball.GetComponent<Rigidbody>().AddForce(GetSwipeVector() * (touchVelocity * Mathf.Sqrt(GetGoalDistance() / throwScaler)), ForceMode.VelocityChange);
                }
                touchBegin = Vector2.zero;
                touchEnd = Vector2.zero;
                startTime = 0;
                endTime = 0;
                Reset = false;
            }
        }
#endif

#if UNITY_ANDROID
        if (Reset == true)
        {
            foreach (Touch item in Input.touches)
            {
                if (item.phase == TouchPhase.Began)
                {
                    Ray start = camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(start, out hit))
                    {
                        //If the player touches the ball, Reset = false, set the initial touch values and start tracking previousPosition.
                        if (hit.collider.gameObject == Ball)
                        {
                            touchBegin = item.position;
                            startTime = Time.time;
                            previousPosition = item.position;
                        }
                    }
                }

                if (item.phase == TouchPhase.Moved || item.phase == TouchPhase.Stationary)
                {
                    //Calculate the power of the touch with 1 being best power to make a goal. 
                    slider.value = GetPower();
                    //If the new posiiton is lower than previous position, reset the swipe. Otherwise update previousPosition.
                    if (item.position.y < previousPosition.y)
                    {
                        previousPosition = item.position;
                        touchBegin = item.position;
                        startTime = Time.time;
                    }
                    else
                    {
                        previousPosition = item.position;
                    }
                }
                //When the touch ends, set release and end touch values.
                if (item.phase == TouchPhase.Ended || item.phase == TouchPhase.Canceled)
                {
                    releaseTime = Time.time;
                    touchEnd = item.position;
                    endTime = Time.time;
                    touchVelocity = GetVelocity();
                    slider.value = GetPower();
                    //See if the power is close enough to use an autoshoot.(still dependent on swipe angle)
                    if (GetPower() > .7 && GetPower() < 1.3)
                    {
                        debug4.text = "Hit";
                        AutoShoot();

                    }
                    //If it's not, only use their input to throw ball. 
                    else
                    {
                        debug4.text = "Maybe";
                        Ball.GetComponent<Rigidbody>().useGravity = true;
                        Ball.GetComponent<Rigidbody>().AddForce(GetSwipeVector() * (touchVelocity * Mathf.Sqrt(GetGoalDistance() / throwScaler)), ForceMode.VelocityChange);
                    }
                    //Reset throw values.
                    touchBegin = Vector2.zero;
                    touchEnd = Vector2.zero;
                    startTime = 0;
                    endTime = 0;
                    Reset = false;
                }
            }
        }
        #endif        
    }


    //Return the current Input divided by the optimum input. The closer the 1, the better the throw.
    public float GetPower()
    {
        return ((GetVelocity() * Mathf.Sqrt(GetGoalDistance() / throwScaler)) / ((averageSwipePercentage / averageSwipeDuration) * Mathf.Sqrt(GetGoalDistance() / throwScaler)));
    }

    //Return the distance between the player and the goal. 
    public float GetGoalDistance()
    {
        return Vector3.Distance(Player.transform.position, Goal.transform.position);
    }

    //Return the velocity of the swipe. 
    public float GetVelocity()
    {
        if(GetDuration() != 0)
        {
            return (Vector2.Distance(touchBegin, touchEnd) / Screen.height) / GetDuration();
        }
        else
        {
            return 0f;
        }
        
    }

    //return the duration of the swipe. 
    public float GetDuration()
    { 
        return endTime - startTime;
    }

    //Return the direction of swipe.
    public Vector3 GetSwipeVector()
    {
       return Ball.transform.forward-new Vector3(0,0, (touchBegin - touchEnd).normalized.x/4);
    }

    //Return the time that has passed since the ball was released.
    public float GetTimeSinceThrown()
    {
        return Time.time - releaseTime;
    }

    //Reset the ball to starting state. 
    public void BallReset()
    {
        releaseTime = Time.time;
        slider.value = 0;
        Ball.GetComponent<Rigidbody>().useGravity = false;
        Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        Ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        Ball.transform.position = shootPosition.position;
        Ball.transform.rotation = shootPosition.rotation;
        previousPosition = new Vector3(1000000000, 0, 0);
        goal = false;
        Reset = true;

    }

    //Shoot the ball to get it very close to make a basket. 
    public void AutoShoot() {
        Ball.GetComponent<Rigidbody>().useGravity = true;
        Ball.GetComponent<Rigidbody>().AddForce(GetSwipeVector()*(averageSwipePercentage / averageSwipeDuration * Mathf.Sqrt( GetGoalDistance()/throwScaler)), ForceMode.VelocityChange);
        releaseTime = Time.time;
        Reset = false;
    }


    //Shoot the ball to get it very close to make a basket. 
    public void AutoShoot2()
    {
        Ball.GetComponent<Rigidbody>().useGravity = true;
        Ball.GetComponent<Rigidbody>().AddForce(Ball.gameObject.transform.forward * (averageSwipePercentage / averageSwipeDuration * Mathf.Sqrt(GetGoalDistance() / throwScaler)), ForceMode.VelocityChange);
        slider.value = 1;
        releaseTime = Time.time;
        Reset = false;
    }




}




// foreach (Touch item in Input.touches)
// {

//  Ray start = camera.ScreenPointToRay(Input.mousePosition);
//  RaycastHit hit;



/*
if (item.phase == TouchPhase.Began)
{
    touchBegin = item.position;
    startTime = Time.time;
}

if(item.phase == TouchPhase.Ended){
    touchEnd = item.position;
    endTime = Time.time;
}
*/

//  }